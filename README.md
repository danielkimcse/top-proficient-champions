# Top Proficient Champions

Project Top Proficient Champions is a script that runs on a list of summoner names and determine what champions they are most proficient with, based on the champion mastery points. This project is designed to look up the teammates during the queue and search up champions that they are most proficient in. The API Method I'm using are: Match-V3, Summoner-V3, and Champion-Mastery-V3.

## Getting Started

    git clone https://bitbucket.org/danielkimcse/top-proficient-champions


### Prerequisites

Python 3

[Python 3 Download](https://www.python.org/downloads/release)


### Installing

    pip3 install pyyaml
    pip3 install pyperclip
    pip3 install riotwatcher

### Running

    ./TPC.py [summoner_names] [-u update]

If [summoner_names] is given, the script will run against them. If not, the script will run against the copied text in the clipboard. 

If [-u update] is given, the script will pull the most recent matches and calculate the win rates for the champions. If not, the script will return the win rates when last updated

### Example 1
This text is copied to my clipboard:

    RiceLegend joined the room.
    revoker joined the room.
    Arcaram joined the room.
    MikeHct joined the room.
    ROFLMFAO LOL joined the room.

Run the script without update

    DKIM-LM:TPC daniel.kim$ ./TPC.py 

    ------------
    |RiceLegend|
    ------------

    Last Update: Tue Mar 20 13:29:13 2018

    lvl 5 Akali (1051110 pt):
    -----------------------------------
    proficiency percentage: 72.66%
    win rate: ??? 

    lvl 6 Yasuo (266091 pt):
    -----------------------------------
    proficiency percentage: 18.39%
    win rate: ??? 

    lvl 5 Kennen (52068 pt):
    -----------------------------------
    proficiency percentage: 3.6%
    win rate: ??? 

    lvl 5 Jarvan IV (39874 pt):
    -----------------------------------
    proficiency percentage: 2.76%
    win rate: ??? 

    ...

Run the script with update

    DKIM-LM:TPC daniel.kim$ ./TPC.py -u
    ------------
    |RiceLegend|
    ------------

    Gathering most recent matches: 95/95 matches processed
    Done!
    Last Update: Tue Mar 20 13:35:22 2018

    lvl 5 Akali (1051110 pt):
    -----------------------------------
    proficiency percentage: 72.66%
    win rate: 59.72%
    total games: 72

    lvl 6 Yasuo (266091 pt):
    -----------------------------------
    proficiency percentage: 18.39%
    win rate: 0.0%
    total games: 2

    lvl 5 Kennen (52068 pt):
    -----------------------------------
    proficiency percentage: 3.6%
    win rate: 33.33%
    total games: 12

    lvl 5 Jarvan IV (39874 pt):
    -----------------------------------
    proficiency percentage: 2.76%
    win rate: 50.0%
    total games: 2

    ...

### Example 2

Run the script without update

    DKIM-LM:TPC daniel.kim$ ./TPC.py RiceLegend

    ------------
    |RiceLegend|
    ------------

    Last Update: Tue Mar 20 13:29:13 2018

    lvl 5 Akali (1051110 pt):
    -----------------------------------
    proficiency percentage: 72.66%
    win rate: ??? 

    lvl 6 Yasuo (266091 pt):
    -----------------------------------
    proficiency percentage: 18.39%
    win rate: ??? 

    lvl 5 Kennen (52068 pt):
    -----------------------------------
    proficiency percentage: 3.6%
    win rate: ??? 

    lvl 5 Jarvan IV (39874 pt):
    -----------------------------------
    proficiency percentage: 2.76%
    win rate: ??? 

    ...

Run the script with update

    DKIM-LM:TPC daniel.kim$ ./TPC.py RiceLegend -u
    ------------
    |RiceLegend|
    ------------

    Gathering most recent matches: 95/95 matches processed
    Done!
    Last Update: Tue Mar 20 13:35:22 2018

    lvl 5 Akali (1051110 pt):
    -----------------------------------
    proficiency percentage: 72.66%
    win rate: 59.72%
    total games: 72

    lvl 6 Yasuo (266091 pt):
    -----------------------------------
    proficiency percentage: 18.39%
    win rate: 0.0%
    total games: 2

    lvl 5 Kennen (52068 pt):
    -----------------------------------
    proficiency percentage: 3.6%
    win rate: 33.33%
    total games: 12

    lvl 5 Jarvan IV (39874 pt):
    -----------------------------------
    proficiency percentage: 2.76%
    win rate: 50.0%
    total games: 2

    ...
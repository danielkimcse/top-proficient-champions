#!/usr/local/bin/python3

#Top Proficient Champions

'''
RiceLegend joined the room.
revoker joined the room.
Arcaram joined the room.
MikeHct joined the room.
ROFLMFAO LOL joined the room.
'''

'''
Champion Points are based on your performance, your team's performance, the length of the game, win vs. loss, and a few other factors. 
'''
from riotwatcher import RiotWatcher
from openpyxl import Workbook

import sys
import os
import yaml
import pyperclip
import re
import time


watcher = RiotWatcher('RGAPI-2ab5bd47-128e-434c-8407-e3ad8ea394b6')
championLimit = 50
matchLimit = 95
my_region = 'na1'
lastPlayed = 1516060800000 #epoch
summoners = []
update = False
if len(sys.argv) < 2 or (len(sys.argv) < 3 and '-u' in sys.argv[-1]): #use pyperclip
	try:
		update = True if '-u' in sys.argv[-1] else False
		lines = pyperclip.paste().split('\n')
		for line in lines:
			summoners.append(re.sub(r'\s', '', line.split(' joined the lobby')[0]))
	except:
		print(pyperclip.paste())
		print('error while using clipboard')
		exit()
else: #use command input
	for summoner in sys.argv: #add summoners from command line
		if 'TPC.py' in summoner: #edge case
			continue
		if '-u' in summoner: #edge case
			update = True
			continue
		summoners.append(summoner)


with open('champions.yaml') as f: #loading champions
	champions = yaml.load(f)

champWR = {'gameId': [],'lastUpdated': time.ctime()} #default template

def findChamp(id): #use champion id to find champion name
	for champion in champions['data'].items():
		if champion[1]['id'] == id:
			return champion[1]['name']

	return '???'

def calculate(mastery,champWR,sheet): #calculate WR and PP and print
	totalChampPoint = 0
	for i in range(len(mastery)): #getting total champion points
		if mastery[i]['lastPlayTime'] < lastPlayed: 
			continue
		champPoint = mastery[i]['championPoints']
		totalChampPoint = totalChampPoint + champPoint

	if totalChampPoint == 0: #proficient champion not played recently
		print("not enough data")
		return
	print('Last Update: ' + champWR['lastUpdated'])
	rowCount = 2
	for i in range(len(mastery)):
		if i > championLimit:
			break 
		champLastPlayed = mastery[i]['lastPlayTime']
		if mastery[i]['lastPlayTime'] < lastPlayed: 
			continue
		champLvl = mastery[i]['championLevel']
		champPoint = mastery[i]['championPoints']
		champId = mastery[i]['championId']
		champName = findChamp(champId)

		print("\nlvl " + str(champLvl) + " " + champName + " ("+str(champPoint)+" pt)"+":\n-----------------------------------")
		print('proficiency percentage: ' + str(round((champPoint/totalChampPoint) * 100, 2))+ "%")
		sheet.cell(row=rowCount,column=1).value = champName
		sheet.cell(row=rowCount,column=2).value = champLvl
		sheet.cell(row=rowCount,column=3).value = champPoint
		sheet.cell(row=rowCount,column=4).value = round((champPoint/totalChampPoint) * 100, 2)
		printWR(champId,champWR,sheet,rowCount)
		rowCount += 1




def printWR(champId,champWR,sheet,rowCount):
	WR = '???'
	if champId in champWR:
		WR = round((champWR[champId]['win']/champWR[champId]['total'])*100, 2)


	print("win rate: " + ('??? ' if WR == '???' else (str(WR) + '%\ntotal games: ' + str(champWR[champId]['total']))))
	sheet.cell(row=rowCount,column=5).value = WR
	if WR != '???':
		sheet.cell(row=rowCount, column=6).value = str(champWR[champId]['total'])

def calculateWR(me):
	matchlist = watcher.match.matchlist_by_account(my_region, me['accountId'])
	matches = matchlist['matches']
	count = 1
	numOfMatches = len(matches) if len(matches) < matchLimit else matchLimit
	for match in matches: #for each match played (100 most recent matches)
		gameId = match['gameId']
		sys.stdout.write("Gathering most recent matches: "+str(count)+"/"+str(numOfMatches)+" matches processed\r")
		sys.stdout.flush()
		if count == matchLimit:
			break
		if gameId in champWR['gameId']: #skip if already in cache
			count += 1
			continue
		champWR['gameId'].append(gameId) #otherwise add to cache

		try:
			match_by_id = watcher.match.by_id(my_region, gameId)

		except:
			print('gameid too old. skipping')
			continue

		participants = match_by_id['participants']
		participantIdentities = match_by_id['participantIdentities']
		participantId = ''

		for pi in participantIdentities: #find partcipantId
			if 'summonerId' not in pi['player']: #if a bot
				continue

			if pi['player']['summonerId'] == myid: 
				participantId = pi['participantId']

		for participant in participants: 
			if participant['participantId'] == participantId: #locate my account
				champId = participant['championId']

				if champId not in champWR: #create new instance champ isn't in the cache
					champWR[champId] = {'total': 0, 'win': 0}

				champWR[champId]['total'] += 1

				if participant['stats']['win']: 
					champWR[champId]['win'] += 1
		count += 1
	print('\nDone!')
	champWR['lastUpdated'] = time.ctime()
	with open(cachePath,'w') as f: #save cache
		yaml.dump(champWR, f, default_flow_style=False)

wb = Workbook()
for summoner in summoners: #iterate through summoner args
	# if 'TPC.py' in sys.argv[i]: #no more summoners
	# 	break

	cachePath = 'cache/' + summoner + '.yaml'

	if os.path.exists(cachePath): #load cache
		with open(cachePath) as f:
			champWR = yaml.load(f)

	# else:
	# 	wb = Workbook()
	# 	sheet = wb.active



	name = summoner
	me = watcher.summoner.by_name(my_region, name)
	myid = me['id']
	mastery = watcher.champion_mastery.by_summoner(my_region, me['id'])

	sheet = wb.create_sheet(name)
	sheet['A1'] = 'Name'
	sheet['B1'] = 'Level'
	sheet['C1'] = 'Points'
	sheet['D1'] = 'Proficiency Percentage'
	sheet['E1'] = 'Win Rate'
	sheet['F1'] = 'Total Games'

	print('\n'+'-'*(len(name)+2))
	print("|" + name + "|")
	print('-'*(len(name)+2)+'\n')

	if update:
		calculateWR(me)
	calculate(mastery, champWR, sheet)

del wb['Sheet']
wb.save('data/' + str(time.ctime()) + '.xlsx')